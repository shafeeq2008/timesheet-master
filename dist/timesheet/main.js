(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _employeetask_employeetask_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./employeetask/employeetask.component */ "./src/app/employeetask/employeetask.component.ts");
/* harmony import */ var _employee_employee_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employee/employee.component */ "./src/app/employee/employee.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', component: _employee_employee_component__WEBPACK_IMPORTED_MODULE_3__["EmployeeListComponent"] },
    { path: 'emptask/:id', component: _employeetask_employeetask_component__WEBPACK_IMPORTED_MODULE_2__["EmployeetaskComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<employee-list></employee-list>-->\r\n\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _employee_employee_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employee/employee.component */ "./src/app/employee/employee.component.ts");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _employeetask_employeetask_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./employeetask/employeetask.component */ "./src/app/employeetask/employeetask.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _employee_employee_component__WEBPACK_IMPORTED_MODULE_5__["EmployeeListComponent"],
                _employeetask_employeetask_component__WEBPACK_IMPORTED_MODULE_7__["EmployeetaskComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            providers: [
                _services_employee_service__WEBPACK_IMPORTED_MODULE_6__["EmployeeService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/employee/employee.component.html":
/*!**************************************************!*\
  !*** ./src/app/employee/employee.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Employee List</h1>\r\n<table class=\"table\">\r\n  <tr>\r\n    <th>ID <a routerLink=\"/emptask\">Dashboard</a></th>\r\n    <th>Code</th>\r\n    <th>Name</th>\r\n    <th>Weekly Effort (Total)</th>\r\n    <th>Weekly Effort (Average)</th>\r\n  </tr>\r\n  <tr *ngFor=\"let item of employees\" (click)=\"onSelect(item)\">\r\n    <td>{{item.id}}</td>\r\n    <td>{{item.code}}</td>\r\n    <td>{{item.name}}</td>\r\n    <td>{{item.totalWeelyEffort}}</td>\r\n    <td>{{item.avgWeelyEffort}}</td>\r\n  </tr>\r\n</table>\r\n\r\n\r\n<!--<table class=\"table\">\r\n  <tr>\r\n    <td>\r\n      <select id=\"EmpSelector\" style=\"width: 100%;\" (change)=\"selectChange($event)\">\r\n        <option *ngFor=\"let emp of emplist\" value=\"{{emp.id}}\">{{emp.name}}</option>\r\n      </select>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <th>Task Name </th>\r\n    <th>Sunday</th>\r\n    <th>Monday</th>\r\n    <th>Tuesday</th>\r\n    <th>Wednesday</th>\r\n    <th>Thursday</th>\r\n    <th>Friday</th>\r\n    <th>Saturday</th>\r\n\r\n  </tr>\r\n  <tr *ngFor=\"let data of employeetask\">\r\n    <td>{{data.id}}</td>\r\n    <td>{{data.sunday}}</td>\r\n    <td>{{data.monday}}</td>\r\n    <td>{{data.tuesday}}</td>\r\n    <td>{{data.wednesday}}</td>\r\n    <td>{{data.thursday}}</td>\r\n    <td>{{data.friday}}</td>\r\n    <td>{{data.saturday}}</td>\r\n\r\n\r\n  </tr>\r\n  <tr><th>Total</th><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>\r\n\r\n</table>\r\n\r\n\r\n<form [formGroup]=\"taskForm\" (ngSubmit)=\"onSubmit()\">\r\n  <table>\r\n\r\n    <tr>\r\n      <td>Task Name </td>\r\n      <td>\r\n        <select formControlName=\"TaskId\" id=\"TaskSelector\" style=\"width: 100%;\">\r\n          <option *ngFor=\"let tsk of tasklist\" value=\"{{tsk.id}}\">{{tsk.name}}</option>\r\n        </select>\r\n      </td>\r\n    </tr>\r\n    <tr>\r\n      <td>Sunday</td>\r\n      <td><input formControlName=\"Sunday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Monday</td>\r\n      <td><input formControlName=\"Monday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Tuesday</td>\r\n      <td><input formControlName=\"Tuesday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Wednesday</td>\r\n      <td><input formControlName=\"Wednesday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Thursday</td>\r\n      <td><input formControlName=\"Thursday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Friday</td>\r\n      <td><input formControlName=\"Friday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Saturday</td>\r\n      <td><input formControlName=\"Saturday\" /></td>\r\n    </tr>\r\n\r\n    <tr><td><input type=\"submit\" /></td></tr>\r\n\r\n  </table>\r\n</form>-->\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/employee/employee.component.scss":
/*!**************************************************!*\
  !*** ./src/app/employee/employee.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1 {\n  color: grey; }\n\n.table {\n  border-collapse: collapse;\n  width: 100%; }\n\n.table td, .table th {\n    border: 1px solid #ddd;\n    padding: 7px; }\n\n.table tr:nth-child(even) {\n    background-color: #f2f2f2; }\n\n.table th {\n    padding-top: 12px;\n    padding-bottom: 12px;\n    text-align: left;\n    background-color: #1c6ca5;\n    color: white; }\n\n.table tr:hover {\n    background-color: #ddd; }\n"

/***/ }),

/***/ "./src/app/employee/employee.component.ts":
/*!************************************************!*\
  !*** ./src/app/employee/employee.component.ts ***!
  \************************************************/
/*! exports provided: EmployeeListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeListComponent", function() { return EmployeeListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EmployeeListComponent = /** @class */ (function () {
    // empid: number;
    // EmployeeTask: EmployeeTask;
    function EmployeeListComponent(employeeService, router) {
        this.employeeService = employeeService;
        this.router = router;
        this.submitted = false;
    }
    //taskForm = new FormGroup({
    //  TaskId: new FormControl(''),
    //  EmployeeId :new FormControl(''),
    //    Sunday: new FormControl(''),
    //    Monday: new FormControl(''),
    //    Tuesday: new FormControl(''),
    //  Wednesday: new FormControl(''),
    //  Thursday: new FormControl(''),
    //    Friday: new FormControl(''),
    //    Saturday: new FormControl('')
    //});
    // get f() { return this.taskForm.controls; }
    // onSubmit() {
    //   alert(this.empid);
    //   this.submitted = true;
    //   this.taskForm.patchValue({
    //     EmployeeId: this.empid
    //     // formControlName2: myValue2 (can be omitted)
    //   });
    //   let EmployeeTask = this.taskForm.value;
    // //  this.EmployeeTask.EmployeeId = this.empid;
    //   this.employeeService.Addemptask(EmployeeTask);
    //  // alert(EmployeeTask.Monday);
    // }
    EmployeeListComponent.prototype.onSelect = function (selectedItem) {
        // this.gatData(selectedItem.id);
    };
    // gatData(id) {
    //   this.empid = id;
    //   this.employeeService.getemptasklist(id).subscribe(data => {
    //     this.employeetask = data;
    //   });
    //   this.employeeService.getallemployees().subscribe(data => {
    //     this.emplist = data;
    //   });
    //   this.employeeService.gettasklist().subscribe(data => {
    //     this.tasklist = data;
    //   });
    // }
    //selectChange (event: any) {
    //   //update the ui
    ////  alert(event.target.value);
    //  this.gatData(event.target.value)
    // }
    EmployeeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.employeeService.getallemployees().subscribe(function (data) {
            _this.employees = data;
        });
    };
    EmployeeListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'employee-list',
            template: __webpack_require__(/*! ./employee.component.html */ "./src/app/employee/employee.component.html"),
            styles: [__webpack_require__(/*! ./employee.component.scss */ "./src/app/employee/employee.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EmployeeListComponent);
    return EmployeeListComponent;
}());



/***/ }),

/***/ "./src/app/employeetask/employeetask.component.html":
/*!**********************************************************!*\
  !*** ./src/app/employeetask/employeetask.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table\">\r\n  <tr>\r\n    <td>\r\n      <select id=\"EmpSelector\" style=\"width: 100%;\" (change)=\"selectChange($event)\">\r\n        <option *ngFor=\"let emp of emplist\" value=\"{{emp.id}}\">{{emp.name}}</option>\r\n      </select>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <th>Task Name </th>\r\n    <th>Sunday</th>\r\n    <th>Monday</th>\r\n    <th>Tuesday</th>\r\n    <th>Wednesday</th>\r\n    <th>Thursday</th>\r\n    <th>Friday</th>\r\n    <th>Saturday</th>\r\n\r\n  </tr>\r\n  <tr *ngFor=\"let data of employeetask\">\r\n    <td>{{data.id}}</td>\r\n    <td>{{data.sunday}}</td>\r\n    <td>{{data.monday}}</td>\r\n    <td>{{data.tuesday}}</td>\r\n    <td>{{data.wednesday}}</td>\r\n    <td>{{data.thursday}}</td>\r\n    <td>{{data.friday}}</td>\r\n    <td>{{data.saturday}}</td>\r\n\r\n\r\n  </tr>\r\n  <tr><th>Total</th><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>\r\n\r\n</table>\r\n\r\n\r\n<form [formGroup]=\"taskForm\" (ngSubmit)=\"onSubmit()\">\r\n  <table>\r\n\r\n    <tr>\r\n      <td>Task Name </td>\r\n      <td>\r\n        <select formControlName=\"TaskId\" id=\"TaskSelector\" style=\"width: 100%;\">\r\n          <option *ngFor=\"let tsk of tasklist\" value=\"{{tsk.id}}\">{{tsk.name}}</option>\r\n        </select>\r\n      </td>\r\n    </tr>\r\n    <tr>\r\n      <td>Sunday</td>\r\n      <td><input formControlName=\"Sunday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Monday</td>\r\n      <td><input formControlName=\"Monday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Tuesday</td>\r\n      <td><input formControlName=\"Tuesday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Wednesday</td>\r\n      <td><input formControlName=\"Wednesday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Thursday</td>\r\n      <td><input formControlName=\"Thursday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Friday</td>\r\n      <td><input formControlName=\"Friday\" /></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Saturday</td>\r\n      <td><input formControlName=\"Saturday\" /></td>\r\n    </tr>\r\n\r\n    <tr><td><input type=\"submit\" /></td></tr>\r\n\r\n  </table>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/employeetask/employeetask.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/employeetask/employeetask.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employeetask/employeetask.component.ts":
/*!********************************************************!*\
  !*** ./src/app/employeetask/employeetask.component.ts ***!
  \********************************************************/
/*! exports provided: EmployeetaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeetaskComponent", function() { return EmployeetaskComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmployeetaskComponent = /** @class */ (function () {
    function EmployeetaskComponent(employeeService, router) {
        this.employeeService = employeeService;
        this.router = router;
        this.submitted = false;
        this.taskForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            TaskId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            EmployeeId: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Sunday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Monday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Tuesday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Wednesday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Thursday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Friday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            Saturday: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
    }
    Object.defineProperty(EmployeetaskComponent.prototype, "f", {
        get: function () { return this.taskForm.controls; },
        enumerable: true,
        configurable: true
    });
    EmployeetaskComponent.prototype.onSubmit = function () {
        alert(this.empid);
        this.submitted = true;
        this.taskForm.patchValue({
            EmployeeId: this.empid
            // formControlName2: myValue2 (can be omitted)
        });
        var EmployeeTask = this.taskForm.value;
        //  this.EmployeeTask.EmployeeId = this.empid;
        this.employeeService.Addemptask(EmployeeTask);
        // alert(EmployeeTask.Monday);
    };
    EmployeetaskComponent.prototype.onSelect = function (selectedItem) {
        this.gatData(selectedItem.id);
    };
    EmployeetaskComponent.prototype.gatData = function (id) {
        var _this = this;
        this.empid = id;
        this.employeeService.getemptasklist(id).subscribe(function (data) {
            _this.employeetask = data;
        });
        this.employeeService.getallemployees().subscribe(function (data) {
            _this.emplist = data;
        });
        this.employeeService.gettasklist().subscribe(function (data) {
            _this.tasklist = data;
        });
    };
    EmployeetaskComponent.prototype.selectChange = function (event) {
        //update the ui
        //  alert(event.target.value);
        this.gatData(event.target.value);
    };
    EmployeetaskComponent.prototype.ngOnInit = function () {
        this.empid = this.router.snapshot.paramMap.get("id");
        this.gatData(this.empid);
        //this.employeeService.getallemployees().subscribe(data => {
        //  this.employees = data;
        //});
    };
    EmployeetaskComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-employeetask',
            template: __webpack_require__(/*! ./employeetask.component.html */ "./src/app/employeetask/employeetask.component.html"),
            styles: [__webpack_require__(/*! ./employeetask.component.scss */ "./src/app/employeetask/employeetask.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], EmployeetaskComponent);
    return EmployeetaskComponent;
}());



/***/ }),

/***/ "./src/app/services/employee.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/employee.service.ts ***!
  \**********************************************/
/*! exports provided: EmployeeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeService", function() { return EmployeeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EmployeeService = /** @class */ (function () {
    function EmployeeService(http) {
        this.http = http;
        this.baseapi = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl;
    }
    EmployeeService.prototype.getallemployees = function () {
        return this.http.get(this.baseapi + "/employee/getall");
    };
    EmployeeService.prototype.getemptasklist = function (id) {
        return this.http.get(this.baseapi + "/employeetask/GetTasklist?empid=" + id);
    };
    EmployeeService.prototype.gettasklist = function () {
        return this.http.get(this.baseapi + "/task/GetTask");
    };
    EmployeeService.prototype.Addemptask = function (emptask) {
        this.http.post(this.baseapi + "/employeetask/Create", emptask).subscribe(function (result) {
            // alert(result.json() as number);
            // this.apivalues = result.json() as string[]
        });
        //  alert("hello" + Users.UserName);
    };
    EmployeeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], EmployeeService);
    return EmployeeService;
}());



/***/ }),

/***/ "./src/environments/environment.prod":
/*!*******************************************!*\
  !*** ./src/environments/environment.prod ***!
  \*******************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
  production: true,
  apiUrl : "https://localhost:44391/api/v1"
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! G:\api\timesheet-master\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map