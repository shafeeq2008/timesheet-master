import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallemployees() {
        return this.http.get(this.baseapi + "/employee/getall");
    }

  getemptasklist(id) {
    return this.http.get(this.baseapi + "/employeetask/GetTasklist?empid="+id);
  }
  gettasklist() {
    return this.http.get(this.baseapi + "/task/GetTask");
  }

  Addemptask(emptask) {
    this.http.post(this.baseapi + "/employeetask/Create", emptask).subscribe(result => {
     // alert(result.json() as number);
      // this.apivalues = result.json() as string[]
    });

  //  alert("hello" + Users.UserName);
  }
}
