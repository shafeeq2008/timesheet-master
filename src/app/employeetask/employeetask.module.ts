import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { employeetaskRoutingModule } from './employeetask-routing.module';
import { EmployeetaskComponent } from './employeetask.component';

@NgModule({
  imports: [CommonModule, employeetaskRoutingModule],
  declarations: [EmployeetaskComponent]
})
export class employeetaskModule { }
