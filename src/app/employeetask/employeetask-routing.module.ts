import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeetaskComponent } from './employeetask.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeetaskComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class employeetaskRoutingModule { }
