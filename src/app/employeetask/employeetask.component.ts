import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EmployeeTask } from '../Model/EmployeeTask';

@Component({
  selector: 'app-employeetask',
  templateUrl: './employeetask.component.html',
  styleUrls: ['./employeetask.component.scss']
})
export class EmployeetaskComponent implements OnInit {

  employees: any;
  employeetask: any;
  emplist: any;
  tasklist: any;
  submitted = false;
  empid: string;

  EmployeeTask: EmployeeTask;
  constructor(private employeeService: EmployeeService, private router:  ActivatedRoute) { }
  taskForm = new FormGroup({
    TaskId: new FormControl(''),
    EmployeeId: new FormControl(''),
    Sunday: new FormControl(''),
    Monday: new FormControl(''),
    Tuesday: new FormControl(''),
    Wednesday: new FormControl(''),
    Thursday: new FormControl(''),
    Friday: new FormControl(''),
    Saturday: new FormControl('')
  });

  get f() { return this.taskForm.controls; }

  onSubmit() {
   // alert(this.empid);
    this.submitted = true;
    this.taskForm.patchValue({
      EmployeeId: this.empid
      // formControlName2: myValue2 (can be omitted)
    });
    let EmployeeTask = this.taskForm.value;
    //  this.EmployeeTask.EmployeeId = this.empid;
    this.employeeService.Addemptask(EmployeeTask);
    // alert(EmployeeTask.Monday);
    this.gatData(this.empid);
  }


  onSelect(selectedItem: any) {
    this.gatData(selectedItem.id);
  }

  gatData(id) {
    this.empid = id;
    this.employeeService.getemptasklist(id).subscribe(data => {
      this.employeetask = data;
    });
   
    this.employeeService.gettasklist().subscribe(data => {
      this.tasklist = data;
    });
  }
  selectChange(event: any) {

    this.empid = event.target.value;

    //update the ui
    //  alert(event.target.value);

    this.gatData(event.target.value)
  }

  ngOnInit() {

    this.empid = this.router.snapshot.paramMap.get("id");

     this.employeeService.getallemployees().subscribe(data => {
      this.emplist = data;


    });

    this.gatData(this.empid);


        //this.employeeService.getallemployees().subscribe(data => {
        //  this.employees = data;
        //});

      }

  

}
