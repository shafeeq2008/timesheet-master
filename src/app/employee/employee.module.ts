import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { employeeRoutingModule } from './employee.routing.module';
import { EmployeeListComponent } from './employee.component';

@NgModule({
  imports: [CommonModule, employeeRoutingModule],
  declarations: [EmployeeListComponent]
})
export class employeeModule { }
