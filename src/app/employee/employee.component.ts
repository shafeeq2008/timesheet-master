import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';
//import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EmployeeTask } from '../Model/EmployeeTask';


@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
  employees: any;
  employeetask: any;
  emplist: any;
  tasklist: any;
  submitted = false;
 // empid: number;

 // EmployeeTask: EmployeeTask;

  constructor(private employeeService: EmployeeService, private router: Router) { }
  //taskForm = new FormGroup({
  //  TaskId: new FormControl(''),
  //  EmployeeId :new FormControl(''),
  //    Sunday: new FormControl(''),
  //    Monday: new FormControl(''),
  //    Tuesday: new FormControl(''),
  //  Wednesday: new FormControl(''),
  //  Thursday: new FormControl(''),
  //    Friday: new FormControl(''),
  //    Saturday: new FormControl('')
    //});
  


 // get f() { return this.taskForm.controls; }

 // onSubmit() {
 //   alert(this.empid);
 //   this.submitted = true;
 //   this.taskForm.patchValue({
 //     EmployeeId: this.empid
 //     // formControlName2: myValue2 (can be omitted)
 //   });
 //   let EmployeeTask = this.taskForm.value;
 // //  this.EmployeeTask.EmployeeId = this.empid;
 //   this.employeeService.Addemptask(EmployeeTask);
 //  // alert(EmployeeTask.Monday);
   
 // }


  onSelect(selectedItem: any) {
    this.router.navigate(['/emptask', selectedItem.id]);
   // this.gatData(selectedItem.id);
    }

 // gatData(id) {
 //   this.empid = id;
 //   this.employeeService.getemptasklist(id).subscribe(data => {
 //     this.employeetask = data;
 //   });
 //   this.employeeService.getallemployees().subscribe(data => {
 //     this.emplist = data;


 //   });
 //   this.employeeService.gettasklist().subscribe(data => {
 //     this.tasklist = data;
 //   });
 // }
 //selectChange (event: any) {
 //   //update the ui
 ////  alert(event.target.value);

 //  this.gatData(event.target.value)
 // }

    ngOnInit() {
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
        });
    
    }
}
