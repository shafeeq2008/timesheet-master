import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee.component';
import { EmployeetaskComponent } from '../employeetask/employeetask.component';

const routes: Routes = [
  
     { path: 'emptask', component: EmployeetaskComponent }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class employeeRoutingModule { }
