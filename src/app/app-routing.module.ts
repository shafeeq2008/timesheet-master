import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EmployeetaskComponent } from './employeetask/employeetask.component';
import { EmployeeListComponent } from './employee/employee.component';


const routes: Routes = [
  { path: '', component: EmployeeListComponent },

  { path: 'emptask/:id', component: EmployeetaskComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
